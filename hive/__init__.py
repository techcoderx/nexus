# coding=utf-8

"""Nexus: microservice powering social networks on the Blurt blockchain.

Nexus is a "consensus interpretation" layer for the Blurt blockchain,
maintaining the state of social features such as post feeds, follows,
and communities. Written in Python, it synchronizes an SQL database
with chain state, providing developers with a more flexible/extensible
alternative to the raw steemd API.
"""
